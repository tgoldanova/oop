x = 10

def foo():
    arr = [1, (0, 1), 'ef', [1, 2]]
    a, b, c, d = arr
    global x

print(foo.__code__.co_nlocals)
print(foo.__code__.co_varnames)
