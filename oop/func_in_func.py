def func():

    def sum(*args, **kwargs):
        sum = 0
        if len(args) > 0:
            for i in args:
                sum += i
        if len(kwargs) > 0:
            for i in kwargs.keys():
                sum += kwargs[i]
        return sum

    return sum

f = func()
print(f(1, 2, 3, 4 ,5, x =10, v = 5))