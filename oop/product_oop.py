class Product:
    def __init__(self, p_type, p_name, p_price):
        self.p_type = p_type
        self.p_name = p_name
        self.p_price = p_price
        self.p_discount = 0
        
class NewProduct(Product):
    def __init__(self, p_type, p_name, p_price, amount):
        Product.__init__(self, p_type, p_name, p_price)
        self.amount = amount

class ProductStore:
    def __init__(self):
        self.storage = []
        self.income = 0
    
    def add(self, prod, n):
        new = NewProduct(prod.p_type, prod.p_name, prod.p_price, n)
        for i in self.storage:
            if i.p_name == new.p_name:
                i.amount += n
                break
        else:
            self.storage.append(new)          

    def set_discount(self, identifier, percent, identifier_type = 'name'):
        sale = int(percent.replace('%', ''))/100
        if identifier_type == 'name':
            for i in identifier:
                for j in self.storage:
                    if i in j.p_name:
                        j.p_discount = sale
                        print(j.p_discount)
        elif identifier_type == 'type':
            for i in identifier:
                for j in self.storage:
                    if i in j.p_type:
                        j.discount = sale
        else:
            raise ValueError("Нет такого типа идентификатора")

    
    def sell(self, name, n):
        for i in self.storage:
            if i.p_name == name:
                if i.amount < n:
                    raise ValueError('Недостаточно товара для продажи')
                else:
                    i.amount -= n
                    self.income += n * (i.p_price - i.p_price* i.p_discount)
                    break
        else:
            raise ValueError('Нет такого товара')

    def get_all_products(self):
        available = []
        for i in self.storage:
            if i.amount > 0:
                available.append((i.p_name, i.amount, i.p_type, i.p_price - i.p_price*i.p_discount, str(int(i.p_discount*100))+'%'))
        return available

    def get_product_info(self, name):
        for i in self.storage:
            if i.p_name == name:
               return (i.p_name, i.amount, i.p_price)
        else:
            raise ValueError('Нет такого товара')

    def get_real_price(self, name):
        for i in self.storage:
            if i.p_name == name:
                return i.p_price
    
    
p = Product('Sport', 'Football T-Shirt', 100)
p2 = Product('Food', 'Ramen', 1.5)

s = ProductStore()

s.add(p, 10)
s.add(p2, 300)


print(s.storage)
print(s.income)

s.sell('Ramen', 10)

print(s.storage)
print(s.income)

assert s.get_product_info('Ramen') == ('Ramen', 290, 1.5)
s.set_discount(['Ramen'], '30%', 'name')

print(s.get_product_info('Ramen'))
s.sell('Ramen', 10)
print(s.income)

set_p = s.get_all_products()
for i in set_p:
    print(i)

print(s.get_real_price('Ramen'))

