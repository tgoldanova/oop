class TVController:
    def __init__(self, channels):
        self.channels = channels
        self.turned = self.channels[0]

    def first_channel(self):
        self.turned = self.channels[0]
        return self.turned

    def last_channel(self):
        self.turned = self.channels[-1]
        return self.turned

    def turn_channel(self, N):
        self.turned = self.channels[N - 1]
        return self.turned

    def next_channel(self):
        if self.turned == self.channels[-1]:
            self.turned = self.turned[0]
        else:
            i = self.channels.index(self.turned)
            self.turned = self.channels[i + 1]
        return self.turned
    
    def previous_channel(self):
        if self.turned == self.channels[0]:
            self.turned = self.turned[-1]
        else:
            i = self.channels.index(self.turned)
            self.turned = self.channels[i - 1]
        return self.turned

    def current_channel(self):
        return self.turned

    def is_exist(self, n):
        if type(n) == str:
            if n in self.channels:
                return 'Yes'
            else: 
                return 'No'

        elif type(n) == int:
            if n in range(1, len(self.channels)+1):
                return 'Yes'
            else:
                return 'No'


CHANNELS = ['BBC', 'Discovery', 'TV1000']

controller = TVController(CHANNELS)

assert controller.first_channel() == 'BBC'
assert controller.last_channel() == 'TV1000'
assert controller.turn_channel(1) == 'BBC'
assert controller.next_channel() == 'Discovery'
assert controller.previous_channel() == 'BBC'
assert controller.current_channel() == 'BBC'
assert controller.is_exist(4) == 'No'
assert controller.is_exist('BBC') == 'Yes'

