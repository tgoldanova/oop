class Dog:
    def __init__(self, age, age_factor = 7):
        self.dogs_age = age
        self.factor = age_factor

    def human_age(self):
        return self.dogs_age * self.factor

d = Dog(3)
print(d.human_age())