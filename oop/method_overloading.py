class Animal:
    def talk(self):
        print('RRRrrrR!!!')

    def talk_animal(self, animal):
        animal.talk()


class Dog(Animal):
    def talk(self):
        print('woof woof')

class Cat(Animal):
    def talk(self):
        print('meow')

c = Cat()
d = Dog()

a = Animal()
a.talk_animal(d)
a.talk_animal(c)