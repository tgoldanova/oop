class Person:
    def __init__(self, f_name, l_name, age):
        self.fname = f_name
        self.lname = l_name
        self.age = age
    
    def talk(self):
        print(f'Hello, my name is {self.fname} {self.lname} and I`m {self.age} years old.')

p = Person('Carl', 'Jonson', 26)
p.talk()