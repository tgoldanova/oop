class Library:
    def __init__(self, name):
        self.l_name = name
        self.l_books = []
        self.l_authors = []
    
    def __str__(self):
        authors = [i.a_name for i in self.l_authors]
        books = [i.b_name for i in self.l_books]
        return f'Library: {self.l_name}, authors: {authors}, books: {books}'

    def __repr__(self):
        authors = [i.a_name for i in self.l_authors]
        books = [i.b_name for i in self.l_books]
        return f'Library: {self.l_name}, authors: {authors}, books: {books}'

    def __len__(self):
        n = 0
        for i in self.l_books:
            n += 1
        return n

    def new_book(self, name, year, author):
        new = Book(name, year, author)
        author.a_books.append(new)
        self.l_books.append(new)
        self.l_authors.append(new.b_author)

    def group_by_author(self, author):
        books = []
        for i in self.l_books:
            if i.b_author.a_name == author:
                books.append(i.b_name)
        return books

    def group_by_year(self, year):
        books = []
        for i in self.l_books:
            if i.b_year == year:
                books.append(i.b_name)
        return books

class Book:
    def __init__(self, name, year, author):
        self.b_name = name
        self.b_year = year
        self.b_author = author

    def __str__(self):
        return f'Book: {self.b_name}, author: {self.b_author.a_name}, year: {self.b_year}'

    def __repr__(self):
        return f'Book: {self.b_name}, author: {self.b_author.a_name}, year: {self.b_year}'

class Author:
    def __init__(self, name, country,  birthday):
        self.a_name = name
        self.a_country = country
        self.a_birthday= birthday
        self.a_books = []

    def __str__(self):
        books = [i.b_name for i in self.a_books]
        return f'Name: {self.a_name}, country: {self.a_country}, birthday: {self.a_birthday}, books: {books}'
    
    def __repr__(self):
        books = [i.b_name for i in self.a_books]
        return f'Name: {self.a_name}, country: {self.a_country}, birthday: {self.a_birthday}, books: {books}'

l = Library('lib')

a1 = Author('J.D.Salinger', 'USA', 1900)
l.new_book('The catcher in the rye', 1940, a1)

a2 = Author('O. Wild', 'UK', 1854)
l.new_book('The picture of Dorian Gray', 1890, a2)

print(l.group_by_author('O. Wild'))
print(l.group_by_year(1940))
print(len(l))
