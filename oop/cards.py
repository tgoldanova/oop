import random

class Card:
    def __init__(self, suit, val):
        self.suit = suit
        self.val = val
    
class Deck:
    def __init__(self):
        self.deck = []
        suits = ['heart', 'diamonds', 'clubs', 'spades']
        values = [2, 3, 4, 5, 6, 7, 8, 9, 10, 'joker', 'ace', 'king', 'queen', 'jack']
        for i in values:
            for j in suits:
                self.deck.append(Card(j, i))
            
    def shuffle(self):
        random.shuffle(self.deck)

    def give_one_card(self):
        card = random.choice(self.deck)
        self.deck.remove(card)
        return (card.val, card.suit)

    def show_rest(self):
        for i in self.deck:
            print(i.val, i.suit)

c = Deck()

c.shuffle()
for i in c.deck:
    print(i.val, i.suit)
print('\ngive card:')
print(c.give_one_card())
print('\nrest:')
c.show_rest()

