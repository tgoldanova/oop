class Mathematician:
    def square_nums(self, integers):
        res = []
        for i in integers:
            res.append(i*i)
        return res
    
    def remove_positives(self, integers):
        res = []
        for i in integers:
            if i < 0:
                res.append(i)
        return res
    
    def filter_leaps(self, years):
        res = []
        for i in years:
            if i % 4 == 0:
                res.append(i)
        return res
                

m = Mathematician()

print(m.square_nums([1, 2, 3, 4, 5]))
print(m.remove_positives([-1, 3, -4, 6]))
print(m.filter_leaps([2001, 1884, 1995, 2003, 2020]))