import datetime

class Person:
    lesson_duration = 0.45

    def __init__(self, f_name, l_name, status, birth_year, work_hours):
        self.first_name = f_name
        self.last_name = l_name
        self.status = status
        self.birth_year = birth_year
        self.age = self.count_age()
        self.work_hours = work_hours

        self.lessons_number = int(work_hours / Person.lesson_duration)

    def count_age(self):
        a = datetime.datetime.now().year - self.birth_year
        return a
        
    def report(self, n = 'full'):
        if n == 'full':
            print(f"Имя: {self.last_name} {self.first_name}")
            print(f"Статус: {self.status}")
            print(f"Возраст: {self.age}")
            print(f"Количество уроков в неделю: {self.lessons_number} (часов: {self.work_hours})")
        elif n == 'short':
            print(f"Имя {self.last_name} {self.first_name}")
            print(f"Статус: {self.status}")

class Student(Person):
    def __init__(self, n_class, marks, class_teacher):
        self.n_class = n_class
        self.marks = marks
        self.average_score = self.count_average_score()
        self.class_teacher = class_teacher

    def count_average_score(self):
        a_score = 0
        for i in self.marks:
            a_score += i
        a_score = a_score / len(self.marks)
        return a_score
        
    def is_excellent_student(self):
        if self.average_score >= 10:
            print(f"Имя {self.last_name} {self.first_name}")
            print(f"Средний балл: {self.average_score}")
            print(f"Статус: отличник")

class Teacher(Person):
    def __init__(self, salary, expirience,  subject):
        self.base_salary = salary
        self.expirience = expirience
        self.extra = self.extra_money()
        self.salary = self.base_salary + self.extra
        self.subject = subject

    def extra_money(self):
        if 20 < self.expirience < 30:
            extr = self.base_salary * 0.2
            return extr
        elif self.expirience > 30:
            extr = self.base_salary * 0.3
            return extr
        else:
            return self.base_salary
        
    