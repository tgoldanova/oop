class Product:
    def __init__(self, type, name, price):
        self.type = type
        self.name = name
        self.price = price

class ProductStore():
    def __init__(self):
        self.assort = []
        self.sold = []

    def add(self, product, amount):
        products = {}
        products['prod'] = product
        products['amount'] = amount
        self.assort.append(products)

    def set_discount(self, identifier, percent, identifier_type = 'name'):
        sale = int(percent.replace('%', ''))/100
        if identifier_type == 'name':
            for i in identifier:
                for j in self.assort:
                    if i in j['prod'].name:
                        j['prod'].price -= j['prod'].price*sale
        elif identifier_type == 'type':
            for i in identifier:
                for j in self.assort:
                    if i in j['prod'].type:
                        j['prod'].price -= j['prod'].price*sale
                    
    def sell_product(self, product_name, amount):
        
        for i in self.assort:
            if i['prod'].name == product_name:
                if i['amount'] >= amount:
                    self.sold.append([product_name, amount])
                    i['amount'] -= amount
                else:
                    print('error')

    def get_income(self):
        income = 0
        for i in self.sold:
            for j in self.assort:
                if i[0] == j['prod'].name:
                    income += j['prod'].price * i[1]
        return income


    def get_all_products(self):
        res = []
        for i in self.assort:
            if i['amount'] > 0:
                res.append((i['prod'].name, i['amount'], i['prod'].type, i['prod'].price))
        return res

    def get_product_info(self, product_name):
        for i in self.assort:
            if i['prod'].name == product_name:
                return (i['prod'].name, i['amount'])


p = Product('Sport', 'Football T-Shirt', 100)
p2 = Product('Food', 'Ramen', 1.5)
s = ProductStore()
s.add(p, 10)
s.add(p2, 300)
s.set_discount(['Food'], '30%', 'type')
s.sell_product('Ramen', 270)
s.sell_product('Ramen', 40)
print(s.get_all_products())
print(s.get_product_info('Ramen'))
print(s.get_income())